﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class PosingTool : MonoBehaviour
{
	public List<Transform> joints = new List<Transform>();
}
